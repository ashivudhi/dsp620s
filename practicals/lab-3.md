# Lab 3

The goal of this lab is to implement *remote invocation* in *Ballerina* using [gRPC](https://grpc.io).

## Problem 1

Consider the greeting example in the previous lab. Using the command-line interface for gRPC in Ballerina, define the interface using protocol buffer and implement the greeter and the student.

## Problem 2

You have been tasked with compiling the statistics related to the COVID-19 pandemic. The statistics are provided per region. A regional statistic includes the date, the region name or code, the accumulated number of:
* deaths
* confirmed cases
* recoveries
* tests

You will design this system using gRPC. Your system includes a client and a server. The client can submit regional statistics and can request:
* the latest statistics for a region
* the statistics for a region on a given date
* the latest national statistics
* the national statistics on a given date

Note that the national statistics represent the sum of all regional statistics on a given date. Moreover,  return statistics should include the active cases which represent the confirmed cases minus the recoveries and the deaths.

## Problem 3

Consider a point calculation system for grade 12 learners in Namibia. Your task is to create a system that takes in a learner’s names and the six subjects written plus the symbol obtained, then calculate the total points obtained. The symbols can either be 1-4 or U for High level and A\*-G or U for ordinary level with respective points given in the table below, it is up to your system to figure this out. However, remember that in case the learner failed English, then the learner gets 0 points and a fail.


|Ordinary Level Symbol| Mark|Higher Level Symbol|Mark|
|:--|:--|:--|:--|
|A+|8|1|10|
|A|7|2|9|
|B|6|3|8|
|C|5|4|7|
|D|4|U|0|
|E|3|||
|F|2|||
|G|1|||
|U|0|||

Additional requirements are as follows:
1. Only valid symbols are accepted;
2. After calculation the system should indicate whether the learner is eligible for tertiary provided they have 25 points and at least an E in English;
3. Symbols can be entered in either case (lower or upper case);
4. If a student skipped the exam for a given subject, then a – (dash) is entered.

Furthermore, the system should ask the learners their preferred two fields of study at tertiary and then indicate whether they would be admitted into that field or not. If not admitted the system should provide a reason. Below are all the fields of study at NUST and their admission requirements.

| Field of study/Faculty| Required Points| Additional Requirements|
|---------------------- | -------------- | ---------------------- |
| Management Sciences | 26 points in 5 subjects|D Symbol in English|
|||E symbol in Mathematics|
|Human Sciences| 26 points in 5 subjects| C Symbol in English|
|Computing and Informatics|30 points in 5 subjects| C Symbol in Mathematics|
| Engineering| 37 points in 5 subjects| Mathematics L3|
|||level 4 for English and Physical Science|
| Health and Applied Sciences | 30 points in 5 subjects|C Symbol in English|
|||D symbol in Biology, Mathematics and Physical Science|
| Natural Resources and Spatial Sciences | 30 points in 5 subjects|C Symbol in English|
|||D symbol in Geography and Mathematics|

Example

Consider a student with the following information:

* Full name: Joshua Maponga
* gender: Male
* Subjects and symbols:
   * English: 2
   * Khoekhoegowab: 1
   * Biology: B
   * Chemistry: C
   * Mathematics: B
   * Agriculture: A
* Field of study 1: Engineering Sciences
* Field of study 2: Medicine

The response from your system could be formatted as follows:

Dear Mr. Joshua Maponga, you have 38 points in 5 subjects. Congratulations! you are eligible for tertiary studies in Namibia.

Field of study 1: Unfortunately, you have not been admitted into Engineering Sciences.
	Comment: Physical Science missing on list of subjects

Field of study 2: Medicine is not offered at NUST

Using gRPC, implement the system.